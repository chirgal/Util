package com.chirgal.tcoe;

import java.io.*;
import java.util.Date;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Chirgal Hansdah <chirgal@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 *
 * @author chirgal.hansdah
 */
public class FileUtils {
    /**
     * Delete a file.
     * If file is a Directory, it must be empty to be get deleted.
     * 
     * @param file File/Directory path to be deleted.
     * @return true, if file is deleted else false.
     */
    public boolean delete(File file) {
        if(file.exists()){
        if (file.delete()) {

            System.out.println("Deleted : " + file.getAbsoluteFile());
            return true;
          }
        }
      return false;
                
    }
    
    /**
     * Delete a file , if it is x days older.
     * If file is a Directory, it must be empty to get deleted.
     * 
     * @param file File/Directory path to be deleted.
     * @param days No. of days older.
     * @return 
     */
    public boolean delete(File file, int days) {
        if (days<1){
             throw new RuntimeException("Parametere 'days' cannot "+
                     "be lesser than 1.");
        }
        if(file.exists()){
        long diff = new Date().getTime() - file.lastModified();
        
        if (diff > days * 24 * 60 * 60 * 1000) {
            if (file.delete()) {

                System.out.println("Deleted : " + file.getAbsoluteFile());
                return true;
              }
           }
       }
        
            return false;
    }
    
    /**
     * Delete a file/directory recursively.
     * 
     * @param file path of File/Directory to be deleted.
     */
    public void deleteFile(File file) {
        //System.out.println("Now will search folders and delete files,");
        if (file.isDirectory()) {
            //System.out.println("Date Modified : " + file.lastModified());
            for (File f : file.listFiles()) {
                deleteFile(f);
            }
            delete(file);

        } else {
            delete(file);
        }

    }
    
    /**
     * Delete a file/directory recursively, if it is x days older.
     * 
     * @param file file File/Directory path to be deleted.
     * @param days No. of days older.
     */
    public void deleteOlderFile(File file, int days) {
        //System.out.println("Now will search folders and delete files,");
        if (file.isDirectory()) {
            //System.out.println("Date Modified : " + file.lastModified());
            for (File f : file.listFiles()) {
                deleteOlderFile(f,days);
            }
            delete(file,days);

        } else {
            delete(file,days);
        }

    }
    
    /**
     * Delete all the files and sub-directories within a base directory.
     * The Base directory is not deleted.
     * 
     * @param dir base directory path.
     */
    public void cleanFile(File dir) {
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                deleteFile(f);
            }
        }else{
          throw new RuntimeException("Parametere 'dir' expected a dir path. ");
        }
    }
    
    /**
     * Delete all the files and sub-directories within a base directory.
     * that are x days older.
     * The Base directory is not deleted.
     * 
     * @param dir
     * @param days 
     */ 
    public void cleanOlderFile(File dir, int days) {
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                deleteOlderFile(f,days);
            }
        }else{
           throw new RuntimeException("Parametere 'dir' expected a dir path. ");
        }
    }
    
    public void filterFile(File dir,String regex){
       for(File f:dir.listFiles()){
        if(f.isFile()){
            if(f.getName().matches(regex)){
                System.out.println(f.getName());
                    }
        }else{
           filterFile(f,regex);
        }
       }
    }
    
    
    public static void main(String args[]) {

        //new FileUtils().deleteFiles(new File("/home/chirgal/GitLab/LearnJava/resources/test1"),1);
        //new FileUtils().deleteOlderFile(new File("/home/chirgal/GitLab/LearnJava/resources/test1"),1);
        new FileUtils().filterFile(new File("D:/GitLab/LearnPy"),".*\\.py");

    }
}
