package com.chirgal.tcoe.bd;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import org.apache.log4j.xml.DOMConfigurator;
import org.apache.pig.ExecType;
import org.apache.pig.PigServer;
import org.apache.hadoop.conf.Configuration;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Chirgal Hansdah <chirgal@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 PigRemote execution example for Cloudera
*/
public class PigRemote {

    public static void main(String[] args) throws Exception {
        addSoftwareLibrary(new File("D:\\conf\\fake"));
        Configuration props = new Configuration();

        DOMConfigurator.configure("D:\\tMbuild\\Spark_ExtractPurchaseData_0.1"
                + "\\Spark_ExtractPurchaseData\\log4j.xml");
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("HADOOP_USER_NAME", "chirgal");
        //Properties props = new Properties();
        props.set("fs.default.name", "hdfs://10.119.11.81:8020");
        props.set("yarn.resourcemanager.address", "10.119.11.81:8032");
        props.set("yarn.resourcemanager.scheduler.address",
                "BHUIVL381864D.ad.infosys.com:8030");
        props.set("mapreduce.framework.name", "yarn");
        props.set("mapreduce.app-submission.cross-platform", "true");
        props.set("dfs.client.use.datanode.hostname", "true");
        props.set("yarn.application.classpath", 
                "$HADOOP_CONF_DIR,$HADOOP_COMMON_HOME/*,"
                        + "$HADOOP_COMMON_HOME/lib/*,$HADOOP_HDFS_HOME/*,"
                        + "$HADOOP_HDFS_HOME/lib/*,$HADOOP_MAPRED_HOME/*,"
                        + "$HADOOP_MAPRED_HOME/lib/*,$YARN_HOME/*,"
                        + "$YARN_HOME/lib/*,$HADOOP_YARN_HOME/*,"
                        + "$HADOOP_YARN_HOME/lib/*,"
                        + "$HADOOP_COMMON_HOME/share/hadoop/common/*,"
                        + "$HADOOP_COMMON_HOME/share/hadoop/common/lib/*,"
                        + "$HADOOP_HDFS_HOME/share/hadoop/hdfs/*,"
                        + "$HADOOP_HDFS_HOME/share/hadoop/hdfs/lib/*,"
                        + "$HADOOP_YARN_HOME/share/hadoop/yarn/*,"
                        + "$HADOOP_YARN_HOME/share/hadoop/yarn/lib/*");

        PigServer pigServer = new PigServer(ExecType.MAPREDUCE, props);
        pigServer.registerScript("D:\\scripts\\test.pig");

    }

    private static void addSoftwareLibrary(File file) throws Exception {
        Method method = URLClassLoader.class.getDeclaredMethod("addURL",
                new Class[]{URL.class});
        method.setAccessible(true);
        method.invoke(ClassLoader.getSystemClassLoader(), 
                new Object[]{file.toURI().toURL()});
    }

}
