/*
# To run the program using remote hiveserver in non-kerberos mode, we need the following jars in the classpath
# from hive/build/dist/lib
#     hive-jdbc*.jar
#     hive-service*.jar
#     libfb303-0.9.0.jar
#     libthrift-0.9.0.jar
#     log4j-1.2.16.jar
#     slf4j-api-1.6.1.jar
#     slf4j-log4j12-1.6.1.jar
#     commons-logging-1.0.4.jar
#
#
# To run the program using kerberos secure mode, we need the following jars in the classpath
#     hive-exec*.jar
#     commons-configuration-1.6.jar (This is not needed with Hadoop 2.6.x and later).
#  and from hadoop
#     hadoop-core*.jar (use hadoop-common*.jar for Hadoop 2.x)
 */

package com.chirgal.tcoe.bd;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author Chirgal.Hansdah
 */

 
public class HiveJdbcClient {
  private static String driverName = "org.apache.hive.jdbc.HiveDriver";
 
  public static void main(String[] args) throws SQLException {
      LinkedHashMap PMap=new LinkedHashMap();
      
    try {
      Class.forName(driverName);
    } catch (ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      System.exit(1);
    }
    
    Connection con = DriverManager.getConnection("jdbc:hive2://10.119.11.193:10000/test", "", "");
    Statement stmt = con.createStatement();
    String tableName = "testHiveDriverTable";
    stmt.execute("drop table " + tableName);
   stmt.execute("create table " + tableName + " (key int, value string)");
    // show tables
    String sql = "show partitions purchases";
    System.out.println("Running: " + sql);
   ResultSet res = stmt.executeQuery(sql);
    if (res.next()) {
      //System.out.println(res.getString(1).split("/")[0]);
        for(String kv:res.getString(1).split("/")){
            if(PMap.get(kv.split("=")[0])==null){
            PMap.put(kv.split("=")[0],kv.split("=")[1]);
            }
            else{
            PMap.put(kv.split("=")[0],PMap.get(kv.split("=")[0])+","+kv.split("=")[1]);
            
            }
        
        }
    }
      Set entrySet = PMap.entrySet();
      
      
      Iterator i = entrySet.iterator();
      while(i.hasNext()){
      Map.Entry me = (Map.Entry)i.next();
      //System.out.println(PMap.get(me.getKey()));
      System.out.println(me.getValue());
      }
}
}
