package com.chirgal.tcoe.converters;

import org.json.JSONObject;
import org.json.XML;
import java.io.*;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Chirgal Hansdah <chirgal@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

public class XmlToJsonConverter {
    
    private static final int PRETTY_PRINT_INDENT_FACTOR = 4;

    
    public static void main(String args[]) throws IOException {
        //JSONObject xmlJSONObj = XML.toJSONObject(XML_TEXT);
        //String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
        //System.out.println(jsonPrettyPrintString);
        StringBuilder builder=new StringBuilder();
        BufferedReader reader =new BufferedReader(new FileReader(new File("D:/GitLab/LearnJava/resources/sampleXml.xml")));
        String line=null;
        
        while((line=reader.readLine())!=null){
        
        builder.append(line);
        }
        reader.close();
        //System.out.println(builder.toString());
        JSONObject xmlJSONObj = XML.toJSONObject(builder.toString());
        String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
        
        StringReader strReader=new StringReader(jsonPrettyPrintString);
        BufferedReader breader=new BufferedReader(strReader);
        BufferedWriter bWriter=new BufferedWriter(new FileWriter(new File("D:/GitLab/LearnJava/resources/sampleJson.json")));
        String s;
        while((s=breader.readLine())!=null){
        bWriter.write(s);
        bWriter.append("\n");
        
        
        }
        bWriter.close();
        breader.close();
    }
    
}

