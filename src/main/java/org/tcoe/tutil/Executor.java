/*
 * The MIT License
 *
 * Copyright (c) 2016 Chirgal Hansdah <chirgal@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.tcoe.tutil;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Class for executing system commands
 *
 *
 */
public class Executor {

    private ProcessBuilder builder;
    private Process process;
    private StdOutStreamPrinter stdout;
    private StdErrStreamPrinter stderr;
    private BufferedWriter stdin;

    /**
     * Initializer
     *
     * @param shell "cmd.exe"|"/bin/sh"|"/bin/bash"|"bin/ksh"
     */
    public Executor(String shell) {

        this.builder = new ProcessBuilder(shell);

    }

    /**
     * Executes a command.
     *
     * @param cmd System command to be executed.
     * @return exit code of the command.
     *
     */
public int exec(String cmd) {

        try {
            process = builder.start();
            stdout = new StdOutStreamPrinter(process);
            stderr = new StdErrStreamPrinter(process);
            stdin = new BufferedWriter(new OutputStreamWriter(
                    process.getOutputStream()));

            stdin.write(cmd);
            stdin.newLine();
            stdin.flush();

            stdin.write("exit");
            stdin.newLine();
            stdin.flush();

            stdout.start();
            stderr.start();
            stdout.join(1000);
            stderr.join(1000);

            process.waitFor();

        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
          catch (IOException ex) {
            ex.printStackTrace();
        }

        return process.exitValue();

    }

public Cmd exec(Cmd cmd) {
    Cmd retObj = new Cmd(cmd.getCmdAlias(), cmd.getCmdCndtn(), cmd.getCmdStr());

    int ret = exec(cmd.getCmdStr());
    retObj.setCmdOp(stdout.getBuff().toString());
    retObj.setCmdRet(ret);
    return retObj;

    }

}
