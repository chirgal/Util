/*
 * The MIT License
 *
 * Copyright (c) 2016 Chirgal Hansdah <chirgal@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
//Refer:https://cwiki.apache.org/confluence/display/Hive/HiveServer2+Clients#HiveServer2Clients-JDBC
package org.tcoe.tutil;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class UDR {

    private static boolean dmode = false;
    static final SimpleDateFormat DATE_FORMAT
            = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
    // A property object that can be used in Talend
    public static java.util.Properties ctx = new java.util.Properties();

    /**
     * iniGet: return value by section.option from ini file without
     * substitution.
     *
     *
     * {talendTypes} String
     *
     * {Category} User Defined
     *
     * {param} string("file") input: The config file. {param} string("section")
     * input: The section in config file. {param} string("option") input: The
     * option within section.
     *
     *
     */
    public static String iniGet(String file, String section, String option) {
        try {

            String val = null;
            Wini ini;
            /* Load the ini file. */
            ini = new Wini(new File(file));
            /* Get the section */
            Wini.Section file_section = ini.get(section);
            val = file_section.get(option);
            /* Return the value*/
            return val;

        } catch (InvalidFileFormatException e) {
            //System.out.println("Invalid file format.");
            return null;
        } catch (IOException e) {
            //System.out.println("Problem reading file.");
            return null;
        }
    }

    /**
     * iniGet: return value by section.option from ini file with substitution.
     *
     *
     * {talendTypes} String
     *
     * {Category} User Defined
     *
     * {param} string("file") input: The config file. {param} string("section")
     * input: The section in config file. {param} string("option") input: The
     * option within section.
     *
     *
     */
    public static String iniFetch(String file, String section, String option) {
        if (dmode) {
            UDR.logInfo("----Invoking " + Thread.currentThread().getStackTrace()[1]
                    .getClassName() + "." + Thread.currentThread().getStackTrace()[1].getMethodName());
        }
        try {
            String val = null;
            Wini ini;
            /* Load the ini file. */
            ini = new Wini(new File(file));
            /* Get the section */
            Wini.Section file_section = ini.get(section);
            val = file_section.fetch(option);
            /* Return the value*/
            return val;

        } catch (InvalidFileFormatException e) {
            //System.out.println("Invalid file format.");
            return null;
        } catch (IOException e) {
            //System.out.println("Problem reading file.");
            return null;
        }
    }

    public static String subWithVal(String source, String token, String val) {
        if (dmode) {
            UDR.logInfo("----Invoking " + Thread.currentThread()
                    .getStackTrace()[1].getClassName() + "."
                    + Thread.currentThread()
                    .getStackTrace()[1].getMethodName());
        }
        StringBuilder builder = new StringBuilder(source);
        String token_key = "${" + token.trim() + "}";
        int token_key_index = builder.indexOf(token_key);
        if (token_key_index > -1) {
            return builder.replace(token_key_index, token_key_index
                    + token_key.length(), val).toString();
        } else {
            if (dmode) {
                UDR.logInfo("----Existing " + Thread.currentThread()
                        .getStackTrace()[1].getClassName() + "."
                        + Thread.currentThread().getStackTrace()[1].getMethodName());
            }
            return builder.toString();
        }
    }

    public static String subWithCtx(String source, java.util.Properties context) {
        if (dmode) {
            UDR.logInfo("----Invoked " + Thread.currentThread()
                    .getStackTrace()[1].getClassName() + "." + Thread.currentThread().getStackTrace()[1].getMethodName());
        }
        StringBuilder builder = new StringBuilder(source);
        java.util.Properties prop = new java.util.Properties(context);
        java.util.Enumeration<?> enumeration = prop.propertyNames();
        while (enumeration.hasMoreElements()) {
            String token = (String) enumeration.nextElement();
            String token_key = "${" + token.trim() + "}";
            int token_key_index = builder.indexOf(token_key);
            if (token_key_index > -1) {
                UDR.log("Substituting \"" + token_key + "\" in \"" + builder.toString() + "\" with value \"" + prop.getProperty(token) + "\".");
                builder.replace(token_key_index, token_key_index + token_key.length(), (String) prop.getProperty(token));
                UDR.log("Final string = " + builder.toString() + ".\n");

            }

        }
        if (dmode) {
            UDR.logInfo("----Existing " + Thread.currentThread()
                    .getStackTrace()[1].getClassName() + "." + Thread.currentThread().getStackTrace()[1].getMethodName());
        }
        return builder.toString();
    }

    @SuppressWarnings("finally")
    public static int jdbcCountRow(String dbtype, String dbhost, String dbport, String dbname, String dbtbl, String dbuser, String dbpwd) {
        if (dmode) {
            UDR.logInfo("----Invoked " + Thread.currentThread()
                    .getStackTrace()[1].getClassName() + "." + Thread.currentThread().getStackTrace()[1].getMethodName());
        }
        int count = -1;
        java.util.Properties tmpProp = new java.util.Properties();
        java.util.Properties jdbcProp = new java.util.Properties();
        jdbcProp.put("MYSQL_JDBC_DRIVER", "com.mysql.jdbc.Driver");
        jdbcProp.put("MYSQL_JDBC_URL", "jdbc:mysql://${MYSQL_DB_HOST}:${MYSQL_DB_PORT}/${MYSQL_DB_NAME}");

        tmpProp.put(dbtype.toUpperCase() + "_DB_USER", dbuser);
        tmpProp.put(dbtype.toUpperCase() + "_DB_HOST", dbhost);
        tmpProp.put(dbtype.toUpperCase() + "_DB_PORT", dbport);
        tmpProp.put(dbtype.toUpperCase() + "_DB_NAME", dbname);

        try {
            Class.forName(jdbcProp.getProperty(dbtype.toUpperCase() + "_JDBC_DRIVER"));
        } catch (ClassNotFoundException e) {
            UDR.logErr(e.getMessage());

        }

        UDR.logInfo(dbtype + " JDBC Driver Registered!");
        Connection connection = null;

        try {
            connection = DriverManager
                    .getConnection(UDR.subWithCtx(jdbcProp.getProperty(dbtype.toUpperCase() + "_JDBC_URL"), tmpProp), dbuser, dbpwd);

        } catch (SQLException e) {

            UDR.logErr(e.getMessage());

        }

        if (connection != null) {
            UDR.logInfo("Connection to " + dbtype + " success!");
        } else {
            UDR.logErr("Failed to make connection!");
        }

        try {
            Statement st = connection.createStatement();

            ResultSet res = st.executeQuery("SELECT COUNT(*) FROM " + dbtbl);
            while (res.next()) {
                count = res.getInt(1);
            }
            connection.close();
            UDR.logInfo("Number of rows:" + count);
        } catch (SQLException s) {
            //System.out.println("SQL statement is not executed!");
            UDR.logErr(s.getMessage());

        } finally {
            if (dmode) {
                UDR.logInfo("----Existing " + Thread.currentThread()
                        .getStackTrace()[1].getClassName() + "." + Thread.currentThread().getStackTrace()[1].getMethodName());
            }

            return count;
        }

    }
    public static String getHivePartitions(String tableName) throws SQLException, ClassNotFoundException {
    	String driverName = "org.apache.hive.jdbc.HiveDriver";
    	LinkedHashMap PMap=new LinkedHashMap();
        
        try {
          Class.forName(driverName);
        } catch (ClassNotFoundException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          throw e;
        }
        
        Connection con = DriverManager.getConnection("jdbc:hive2://10.119.11.193:10000/test", "", "");
        Statement stmt = con.createStatement();
        
        // show tables
        String sql = "show partitions "+tableName;
        System.out.println("Running: " + sql);
       ResultSet res = stmt.executeQuery(sql);
        if (res.next()) {
          //System.out.println(res.getString(1).split("/")[0]);
            for(String kv:res.getString(1).split("/")){
                if(PMap.get(kv.split("=")[0])==null){
                PMap.put(kv.split("=")[0],kv.split("=")[1]);
                }
                else{
                PMap.put(kv.split("=")[0],PMap.get(kv.split("=")[0])+","+kv.split("=")[1]);
                
                }
            
            }
        }
          Set entrySet = PMap.entrySet();
          StringBuilder builder=new StringBuilder();
          
          Iterator i = entrySet.iterator();
          while(i.hasNext()){
          Map.Entry me = (Map.Entry)i.next();
          builder.append(me.getKey()).append("=").append(me.getValue());
          //System.out.println(me.getValue());
    	  builder.append("\n");
          }
    	  return builder.toString();
        
    }
    
    public static void hive2Execute(String url,String dbName,String userName,String passwd,String sql) throws SQLException, ClassNotFoundException { 
    	String driverName = "org.apache.hive.jdbc.HiveDriver";
    	try {
    	      Class.forName(driverName);
    	    } catch (ClassNotFoundException e) {
    	      // TODO Auto-generated catch block
    	      e.printStackTrace();
    	      throw e;
    	    }
    	    Connection con = DriverManager.getConnection("jdbc:hive2://"+url+"/"+dbName, userName, passwd);
    	    Statement stmt = con.createStatement();
    	    System.out.println("[INFO ] Executing sql query -\n" + sql+";");
    	    stmt.execute("set hive.exec.dynamic.partition.mode=nonstrict");
    	    stmt.execute(sql);
    	 }   
    /*==============================================================================
     Logging Module
     =============================================================================*/
    public static void logInfo(String msg) {
        UDR.logInfo(msg, true);
    }

    public static void logWarn(String msg) {
        if (dmode) {
            System.out.printf("[%-5s] "
                    + DATE_FORMAT.format(new Date()) + " "
                    + msg + "\n", "WARN");
        }
    }

    public static void logErr(String msg) {
        if (dmode) {
            System.out.printf("[%-5s] "
                    + DATE_FORMAT.format(new Date()) + " "
                    + msg + "\n", "ERROR");
        }
    }

    public static void logContext(String msg, java.util.Properties context) {
        java.util.Properties prop = new java.util.Properties(context);
        java.util.Enumeration<?> enumeration = prop.propertyNames();
        UDR.logInfo(msg);
        while (enumeration.hasMoreElements()) {
            String token = (String) enumeration.nextElement();
            UDR.log(token + "=" + prop.getProperty(token, ""));
        }
    }

    public static void set_dmode(boolean flg) {
        dmode = flg;
    }

    public static void log(String msg) {
        if (dmode && (!msg.isEmpty()) && (msg != null)) {
            System.out.println(msg);
        }
    }

    public static void logInfo(String msg, boolean add_newline) {
        if (dmode) {
            if (add_newline) {
                System.out.printf("[%-5s] "
                        + DATE_FORMAT.format(new Date()) + " "
                        + msg + "\n", "INFO");
            } else {
                System.out.printf("[%-5s] "
                        + DATE_FORMAT.format(new Date()) + " "
                        + msg, "INFO");
            }
        }
    }

}
