/*
 * The MIT License
 *
 * Copyright (c) 2016 Chirgal Hansdah <chirgal@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.tcoe.tutil;

import java.io.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ini4j.Wini;
import org.python.util.PythonInterpreter;
import org.python.core.*;
import org.python.objectweb.asm.Type;

/**
 *
 *
 */
public class Sequencerj {

    private final String IniFileName;
    private final PythonInterpreter interp;
    private String var_section_name;
    private String cmd_section_name;
    private final String conf_section_name = "conf";
    private boolean rerun = true;

    public Sequencerj(String IniFileName) {
        this.IniFileName = IniFileName;
        interp = new PythonInterpreter();
    }

    public int run() {
        int EXIT_CODE = 0;
        try {

            //##Default imports##
            interp.exec("from datetime import datetime");
            interp.exec("import time");
            interp.exec("import os");
            interp.exec("from java.lang import System");

            Iterator iter;
            Set<String> set;
            List<Cmd> list;

            PyBoolean executeFlg = new PyBoolean(true);
            Wini ini = new Wini(new File(this.getIniFileName()));
            Wini.Section conf_section = ini.get(conf_section_name);

            if (ini.containsKey(conf_section_name)) {
                rerun = Boolean.getBoolean(conf_section.get("rerun"));
                var_section_name = conf_section.get("variable_section", "var");
                cmd_section_name = conf_section.get("command_section", 
                        "command");
            } else {
                var_section_name = "var";
                cmd_section_name = "command";
            }

            Wini.Section var_section = ini.get(var_section_name);
            Wini.Section command_section = ini.get(cmd_section_name);

            Properties stateProp = new Properties();
            Properties varProp = new Properties();

            /*==================================================================
             *                Import Section evaluation [Start]           
             *================================================================*/
            String jImports = IniConfigFile
                    .getSectionContent(this.IniFileName, "import");
            if (!jImports.equals("")) {
                interp.exec(jImports);

                PyObject localvars = interp.getLocals();
                for (PyObject item : localvars.asIterable()) {
                    varProp.setProperty(item.toString(),
                            interp.get(item.toString()).toString());
                }
            }

            
            /*==================================================================
             *                Variable Section evaluation [Start]           
             *================================================================*/
            String jCode = IniConfigFile
                    .getSectionContent(this.IniFileName, var_section_name);
            if (!jCode.equals("")) {
                interp.exec(jCode);

                PyObject localvars = interp.getLocals();
                for (PyObject item : localvars.asIterable()) {
                    varProp.setProperty(item.toString(),
                            interp.get(item.toString()).toString());
                }
            }

             
            set = command_section.keySet();
            list = new ArrayList<Cmd>();
            iter = set.iterator();

            try {
                stateProp.load(new FileReader(new File("state.prop")));
                if (rerun && (Integer.parseInt(stateProp.getProperty(
                        "last_cmdRet")) != 0)) {
                    FileInputStream fileIn = new FileInputStream("cmd.ser");
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    list = (ArrayList<Cmd>) in.readObject();
                    iter = list.iterator();
                    in.close();
                    fileIn.close();

                    System.out.println("[INFO ] " + UDR.DATE_FORMAT
                            .format(new Date()) + " Restarting previous state");
                }

            } catch (IOException i) {
                System.err.println("[ERROR] " + UDR.DATE_FORMAT
                        .format(new Date()) + " Unable to restart previous"
                        + " state");
                System.err.println(i.toString());
            } catch (ClassNotFoundException ex) {
                System.err.println("[ERROR] " + UDR.DATE_FORMAT
                        .format(new Date()) + " Unable to load Sequnse(s)");
                System.err.println(ex.toString());
            }

            //for each line of command section,create a Cmd object
            while (iter.hasNext() && (!rerun)) {
                System.out.println("[INFO ] " + UDR.DATE_FORMAT.
                        format(new Date()) + " Loading command");
                String cmdAlias = (String) iter.next();
                String value = (String) command_section.get(cmdAlias);
                String cmdCondtn = StringUtils.substringBetween(value,
                        "<if%", "%>"); //get condition for command execution
                String cmdStr = null;
                if (cmdCondtn != null) {
                    // get command  string after <% %>
                    cmdStr = StringUtils.substringAfterLast(value, "%>").trim();
                    // subtitute variables ${var} from context
                    cmdStr = UDR.subWithCtx(cmdStr, varProp);
                } else {

                    cmdStr = UDR.subWithCtx(value, varProp);
                }

                System.out.println("cmdAlias=" + cmdAlias);
                System.out.println("cmdCndtn=" + cmdCondtn);
                System.out.println("cmdStr=" + cmdStr);
                System.out.println();

                Cmd cmd = new Cmd(cmdAlias, cmdCondtn, cmdStr);
                list.add(cmd);
            }

            for (Cmd tcmd0 : list) {

                if (rerun && (!tcmd0.getCmdAlias()
                        .equals(stateProp.getProperty("last_cmdAlias", "")))) {
                    continue;
                }

                //Evaluate execute condition
                if ((!StringUtils.defaultString(tcmd0.getCmdCndtn()).isEmpty()))
                {
                    for (Cmd tcmd : list) {
                        interp.set(tcmd.getCmdAlias(), tcmd);
                    }

                    interp.exec("_eval_flg_out_=" + tcmd0.getCmdCndtn());
                    executeFlg = (PyBoolean) interp.get("_eval_flg_out_");
                    System.out.println("[INFO ] " + UDR.DATE_FORMAT
                            .format(new Date()) + " Evaluated condition for "
                            + tcmd0.getCmdAlias() + ": " + executeFlg);

                } else {
                    executeFlg = new PyBoolean(true);
                }

                //Execute command
                if ("true".equalsIgnoreCase(executeFlg.toString())) {

                    stateProp.setProperty("last_cmdAlias", tcmd0.getCmdAlias());
                    System.out.println("======================================="
                            + "================================================"
                            + "===================");
                    System.out.println("[INFO ] " + UDR.DATE_FORMAT.
                            format(new Date()) + " Executing "
                            + tcmd0.getCmdAlias() + ": " + tcmd0.getCmdStr());
                    System.out.println("======================================="
                            + "================================================"
                            + "===================");

                    //Execute & capture returned Cmd object
                    Cmd retObj = new Executor("cmd.exe").exec(tcmd0);
                    tcmd0.setCmdRet(retObj.getCmdRet());
                    EXIT_CODE = retObj.getCmdRet();

                    stateProp.setProperty("last_cmdRet", Integer.toString(
                            EXIT_CODE));

                    tcmd0.setCmdOp(StringUtils.chomp(retObj.getCmdOp()));

                    System.out.println("[INFO ] " + UDR.DATE_FORMAT
                            .format(new Date()) + " Exit code for "
                            + tcmd0.getCmdAlias() + ": " + EXIT_CODE);

                } else {

                    System.out.println("[INFO ] " + UDR.DATE_FORMAT
                            .format(new Date()) + " Skipping "
                            + tcmd0.getCmdAlias() + ": " + tcmd0.getCmdStr());

                }

            }
            if (EXIT_CODE != 0) {
                stateProp.store(new FileWriter(
                        new File("state.prop")), "Previous run stat. ");
                FileOutputStream fileOut = new FileOutputStream("cmd.ser");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(list);
                out.close();
                fileOut.close();

            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return EXIT_CODE;
    }
    

    public String getIniFileName() {
        return IniFileName;
    }
  public static void main(String args[]) {
        Sequencerj seq = new Sequencerj("D:\\data\\test2.ini");
        //UDR.set_dmode(true);
        System.exit(seq.run());
    }
}
