package org.tcoe.tutil;

import org.python.util.PythonInterpreter;
import org.python.core.*;
import java.io.*;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Chirgal Hansdah <chirgal@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 *
 * @author Chirgal.Hansdah
 */


public class Embedded
{
    public static void main(String[]args) throws PyException, IOException
    {
        BufferedReader terminal;
        PythonInterpreter interp;
        terminal = new BufferedReader(new InputStreamReader(System.in));
        System.out.println ("Hello");
        interp = new PythonInterpreter();
        interp.exec("import sys");
        interp.exec("print sys");
        interp.set("a", new PyInteger(42));
        interp.exec("print a");
        interp.exec("x = 2+2");
        interp.exec("for i in range(3):"
                +"\n   print(i)");
        PyObject x = interp.get("x");
        System.out.println("x: " + x);
        PyObject localvars = interp.getLocals();
        //System.out.println(localvars);
        for (PyObject item : localvars.asIterable()) {
               System.out.println(item+"="+interp.get(item.toString()));
          }
        
        interp.set("localvars", localvars);
        String codeString = "";
        String prompt = ">> ";
        while (true)
        {
            System.out.print (prompt);
            try
            {
                StringBuffer str=new StringBuffer();
             
                codeString = terminal.readLine();
                if (codeString.equals("exit"))
                {
                    System.exit(0);
                    break;
                }
                interp.exec(codeString);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        System.out.println("Goodbye");
    }
}
