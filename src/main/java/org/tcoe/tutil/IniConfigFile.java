/*
 * The MIT License
 *
 * Copyright (c) 2016 Chirgal Hansdah <chirgal@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.tcoe.tutil;

/**
 *
 * @author Chirgal.Hansdah
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Handles Configuration file (INI) operation.
 *
 *
 */
public class IniConfigFile {

    /**
     * Read contents within a section in the INI configuration file.
     *
     * @Param file configuration file path
     * @Param section of the configuration file
     *
     * @Return content string of the section
     * @Throws IOException
     */
    public static String getSectionContent(String file, String section) {

        String strLine = "";
        StringBuffer strBuffer = new StringBuffer("");
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(
                    new FileReader(file));

            String currentSection = "";

            UDR.logInfo("Looking for section: " + "[" + section + "]");

            while ((strLine = bufferedReader.readLine()) != null) {
                //strLine = strLine.trim(); 
                Pattern p;
                Matcher m;
                p = Pattern.compile("(\\[)(\\s*.+\\s*)(\\])");
                m = p.matcher((strLine));

                if (m.matches()) {

                    currentSection = m.group(2);
                    continue;
                }

                if (currentSection.equals(section)) {
                    //strLine = strLine.trim();
                    strBuffer.append(strLine).append('\n');

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return StringUtils.chomp(strBuffer.toString());
    }

}
