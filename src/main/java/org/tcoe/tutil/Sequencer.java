package org.tcoe.tutil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import org.ini4j.Wini;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Chirgal Hansdah <chirgal@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


/**
 *
 *
 */
public class Sequencer {

    private final String IniFileName;

    public Sequencer(String IniFileName) {
        this.IniFileName = IniFileName;
    }

    public int run() {
        int EXIT_CODE = -9999;
        try {

            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("javascript");
            Iterator iter;
            Set<String> set;
            List<Cmd> list;
            boolean executeFlg = true;
            Wini ini = new Wini(new File(this.getIniFileName()));
            Wini.Section var = ini.get("var");
            Wini.Section section = ini.get("test");

            StringBuilder jsStringBuilder = new StringBuilder();
            Properties varProp = new Properties();

// ################## Variable Section evaluation [start] ######################            
            if (var != null) {
                set = var.keySet();
                iter = set.iterator();

                //Evaluates variables 
                if (iter.hasNext()) {
                    System.out.println("--- Evaluating Variable(s)");
                }
                while (iter.hasNext()) {
                    String VarKey = (String) iter.next();
                    jsStringBuilder.append(VarKey.trim()).append("=")
                            .append(var.get(VarKey, "").trim());
                    if (!var.get(VarKey, "").trim().endsWith(";")) {
                        jsStringBuilder.append(";");
                    }

                    engine.eval(jsStringBuilder.toString());
                    jsStringBuilder = new StringBuilder();

                    System.out.println(VarKey + "=" + engine.get(VarKey)
                            .toString());
                    engine.put(VarKey, engine.get(VarKey));
                    varProp.setProperty(VarKey, engine.get(VarKey).toString());
                }
                System.out.println();
            }
// ################## Variable Section evaluation [ end ] ######################

// ################## Command Section execution   [start] ######################            
            set = section.keySet();
            list = new ArrayList<Cmd>();
            iter = set.iterator();

            if (iter.hasNext()) {
                System.out.println("[INFO ] Printing Sequence(s)");
            } else {
                System.out.println("[INFO ] No Sequence/Command to execute");
                return 0;

            }
            //for each line of command section,create a Cmd object
            while (iter.hasNext()) {
                String cmdAlias = (String) iter.next();
                String value = (String) section.get(cmdAlias);
                String cmdCondtn = StringUtils.substringBetween(value,
                        "<%", "%>"); //get condition for command execution
                String cmdStr = null;
                if (cmdCondtn != null) {
                    // get command  string after <% %>
                    cmdStr = StringUtils.substringAfterLast(value, "%>").trim();
                    // subtitute variables ${var} from context
                    cmdStr = UDR.subWithCtx(cmdStr, varProp);
                } else {

                    cmdStr = UDR.subWithCtx(value, varProp);
                }

                System.out.println("cmdAlias=" + cmdAlias);
                System.out.println("cmdCndtn=" + cmdCondtn);
                System.out.println("cmdStr=" + cmdStr);
                System.out.println();

                Cmd cmd = new Cmd(cmdAlias, cmdCondtn, cmdStr);
                list.add(cmd);
            }

            for (Cmd tcmd0 : list) {

                //Evaluate execute condition
                if ((!StringUtils.defaultString(tcmd0.getCmdCndtn()).isEmpty())) {
                    for (Cmd tcmd : list) {
                        engine.put(tcmd.getCmdAlias(), tcmd);
                    }

                    engine.eval("var out=" + tcmd0.getCmdCndtn());
                    executeFlg = (Boolean) engine.get("out");

                }

                //Execute command
                if (executeFlg) {

                    System.out.println("[INFO ] Executing "
                            + tcmd0.getCmdAlias() + ": " + tcmd0.getCmdStr());

                    //Execute & capture returned Cmd object
                    Cmd retObj = new Executor("cmd.exe").exec(tcmd0);
                    tcmd0.setCmdRet(retObj.getCmdRet());
                    EXIT_CODE = retObj.getCmdRet();

                    tcmd0.setCmdOp(StringUtils.chomp(retObj.getCmdOp()));

                    System.out.println("[INFO ] Exit code for "
                            + tcmd0.getCmdAlias() + ": " + EXIT_CODE);

                } else {

                    System.out.println("[INFO ] Skipping "
                            + tcmd0.getCmdAlias() + ": " + tcmd0.getCmdStr());

                }

            }

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ScriptException ex) {
            ex.printStackTrace();
        }
        return EXIT_CODE;
    }
// ################## Command Section execution   [ end ] ######################    

    public String getIniFileName() {
        return IniFileName;
    }

    public static void main(String args[]) {
        Sequencer seq = new Sequencer("D:\\data\\test.ini");
        //UDR.set_dmode(true);
        System.exit(seq.run());
    }

}
