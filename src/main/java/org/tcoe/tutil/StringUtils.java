
package org.tcoe.tutil;
/**
 * Following source code has been  Taken from 
 * org.apache.commons.lang3.StringUtils, under Apache License, Version 2.0.
 */

import java.util.ArrayList;
import java.util.List;


/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */




/**
 * Operations on {@link java.lang.String} that are
 * <code>null</code> safe.

 *
 * @see java.lang.String
 * @author <a href="http://jakarta.apache.org/turbine/">Apache Jakarta Turbine</a>
 * @author <a href="mailto:jon@latchkey.com">Jon S. Stevens</a>
 * @author Daniel L. Rall
 * @author <a href="mailto:gcoladonato@yahoo.com">Greg Coladonato</a>
 * @author <a href="mailto:ed@apache.org">Ed Korthof</a>
 * @author <a href="mailto:rand_mcneely@yahoo.com">Rand McNeely</a>
 * @author Stephen Colebourne
 * @author <a href="mailto:fredrik@westermarck.com">Fredrik Westermarck</a>
 * @author Holger Krauth
 * @author <a href="mailto:alex@purpletech.com">Alexander Day Chaffee</a>
 * @author <a href="mailto:hps@intermeta.de">Henning P. Schmiedehausen</a>
 * @author Arun Mammen Thomas
 * @author Gary Gregory
 * @author Phil Steitz
 * @author Al Chou
 * @author Michael Davey
 * @author Reuben Sivan
 * @author Chris Hyzer
 * @author Scott Johnson
 * @since 1.0
 * @version $Id: StringUtils.java 635447 2008-03-10 06:27:09Z bayard $
 */
public class StringUtils {
    
     /**
  * A String for a space character.
  *
  * @since 3.2
  */
 public static final String SPACE = " ";

 /**
  * The empty String {@code ""}.
  * @since 2.0
  */
 public static final String EMPTY = "";

 /**
  * A String for linefeed LF ("\n").
  *
  * @see <a href="http://docs.oracle.com/javase/specs/jls/se7/html/jls-3.html#jls-3.10.6">JLF: Escape Sequences
  *      for Character and String Literals</a>
  * @since 3.2
  */
 public static final String LF = "\n";

 /**
  * A String for carriage return CR ("\r").
  *
  * @see <a href="http://docs.oracle.com/javase/specs/jls/se7/html/jls-3.html#jls-3.10.6">JLF: Escape Sequences
  *      for Character and String Literals</a>
  * @since 3.2
  */
 public static final String CR = "\r";
 
  /**
  * Represents a failed index search.
  * @since 2.1
  */
 public static final int INDEX_NOT_FOUND = -1;

 /**
  * <p>The maximum size to which the padding constant(s) can expand.</p>
  */
 private static final int PAD_LIMIT = 8192;


  // Substring between
  //-----------------------------------------------------------------------
  /**
   * Gets the String that is nested in between two instances of the
   * same String.
   *
   * A <code>null</code> input String returns <code>null</code>.
   * A <code>null</code> tag returns <code>null</code>.
   *
   * <pre>
   * StringUtils.substringBetween(null, *)            = null
   * StringUtils.substringBetween("", "")             = ""
   * StringUtils.substringBetween("", "tag")          = null
   * StringUtils.substringBetween("tagabctag", null)  = null
   * StringUtils.substringBetween("tagabctag", "")    = ""
   * StringUtils.substringBetween("tagabctag", "tag") = "abc"
   * </pre>
   *
   * @param str  the String containing the substring, may be null
   * @param tag  the String before and after the substring, may be null
   * @return the substring, <code>null</code> if no match
   * @since 2.0
   */
    // Taken from org.apache.commons.lang3.StringUtils, Apache License, Version 2.0
  public static String substringBetween(String str, String tag) {
      return substringBetween(str, tag, tag);
  }

  /**
   * Gets the String that is nested in between two Strings.
   * Only the first match is returned.
   *
   * A <code>null</code> input String returns <code>null</code>.
   * A <code>null</code> open/close returns <code>null</code> (no match).
   * An empty ("") open and close returns an empty string.
   *
   * <pre>
   * StringUtils.substringBetween("wx[b]yz", "[", "]") = "b"
   * StringUtils.substringBetween(null, *, *)          = null
   * StringUtils.substringBetween(*, null, *)          = null
   * StringUtils.substringBetween(*, *, null)          = null
   * StringUtils.substringBetween("", "", "")          = ""
   * StringUtils.substringBetween("", "", "]")         = null
   * StringUtils.substringBetween("", "[", "]")        = null
   * StringUtils.substringBetween("yabcz", "", "")     = ""
   * StringUtils.substringBetween("yabcz", "y", "z")   = "abc"
   * StringUtils.substringBetween("yabczyabcz", "y", "z")   = "abc"
   * </pre>
   *
   * @param str  the String containing the substring, may be null
   * @param open  the String before the substring, may be null
   * @param close  the String after the substring, may be null
   * @return the substring, <code>null</code> if no match
   * @since 2.0
   */
  public static String substringBetween(String str, String open, String close) {
      if (str == null || open == null || close == null) {
          return null;
      }
      int start = str.indexOf(open);
      if (start != -1) {
          int end = str.indexOf(close, start + open.length());
          if (end != -1) {
              return str.substring(start + open.length(), end);
          }
      }
      return null;
  }
  
  
  /**
   * <p>Gets the substring after the last occurrence of a separator.
   * The separator is not returned.</p>
   *
   * <p>A <code>null</code> string input will return <code>null</code>.
   * An empty ("") string input will return the empty string.
   * An empty or <code>null</code> separator will return the empty string if
   * the input string is not <code>null</code>.</p>
   *
   * <pre>
   * StringUtils.substringAfterLast(null, *)      = null
   * StringUtils.substringAfterLast("", *)        = ""
   * StringUtils.substringAfterLast(*, "")        = ""
   * StringUtils.substringAfterLast(*, null)      = ""
   * StringUtils.substringAfterLast("abc", "a")   = "bc"
   * StringUtils.substringAfterLast("abcba", "b") = "a"
   * StringUtils.substringAfterLast("abc", "c")   = ""
   * StringUtils.substringAfterLast("a", "a")     = ""
   * StringUtils.substringAfterLast("a", "z")     = ""
   * </pre>
   *
   * @param str  the String to get a substring from, may be null
   * @param separator  the String to search for, may be null
   * @return the substring after the last occurrence of the separator,
   *  <code>null</code> if null String input
   * @since 2.0
   */
  
  public static String substringAfterLast(String str, String separator) {
      if (isEmpty(str)) {
          return str;
      }
      if (isEmpty(separator)) {
          return "";
      }
      int pos = str.lastIndexOf(separator);
      if (pos == -1 || pos == (str.length() - separator.length())) {
          return "";
      }
      return str.substring(pos + separator.length());
  }
  
// Chomping
//-----------------------------------------------------------------------
/**
 * <p>Removes one newline from end of a String if it's there,
 * otherwise leave it alone.  A newline is &quot;{@code \n}&quot;,
 * &quot;{@code \r}&quot;, or &quot;{@code \r\n}&quot;.</p>
 *
 * <p>NOTE: This method changed in 2.0.
 * It now more closely matches Perl chomp.</p>
 *
 * <pre>
 * StringUtils.chomp(null)          = null
 * StringUtils.chomp("")            = ""
 * StringUtils.chomp("abc \r")      = "abc "
 * StringUtils.chomp("abc\n")       = "abc"
 * StringUtils.chomp("abc\r\n")     = "abc"
 * StringUtils.chomp("abc\r\n\r\n") = "abc\r\n"
 * StringUtils.chomp("abc\n\r")     = "abc\n"
 * StringUtils.chomp("abc\n\rabc")  = "abc\n\rabc"
 * StringUtils.chomp("\r")          = ""
 * StringUtils.chomp("\n")          = ""
 * StringUtils.chomp("\r\n")        = ""
 * </pre>
 *
 * @param str  the String to chomp a newline from, may be null
 * @return String without newline, {@code null} if null String input
 */
/*
 * Taken from org.apache.commons.lang3.StringUtils, under Apache License,
 * Version 2.0
 */
public static String chomp(final String str) {
    char CR='\r'; //Added for org.apache.commons.lang3.CharUtils.CR
    char LF='\n'; //Added for org.apache.commons.lang3.CharUtils.LF
    
    if (isEmpty(str)) {
        return str;
    }

    if (str.length() == 1) {
        final char ch = str.charAt(0);
        //if (ch == CharUtils.CR || ch == CharUtils.LF) {
          if (ch == CR || ch == LF) {
            return EMPTY;
        }
        return str;
    }

    int lastIdx = str.length() - 1;
    final char last = str.charAt(lastIdx);

    if (last == LF) {
        if (str.charAt(lastIdx - 1) == CR) {
            lastIdx--;
        }
    } else if (last != CR) {
        lastIdx++;
    }
    return str.substring(0, lastIdx);
}
// Defaults
 //-----------------------------------------------------------------------
 /**
  * <p>Returns either the passed in String,
  * or if the String is {@code null}, an empty String ("").</p>
  *
  * <pre>
  * StringUtils.defaultString(null)  = ""
  * StringUtils.defaultString("")    = ""
  * StringUtils.defaultString("bat") = "bat"
  * </pre>
  *
  * @see ObjectUtils#toString(Object)
  * @see String#valueOf(Object)
  * @param str  the String to check, may be null
  * @return the passed in String, or the empty String if it
  *  was {@code null}
  */
   public static String defaultString(final String str) {
     return str == null ? EMPTY : str;
   }

  // Empty checks
  //-----------------------------------------------------------------------
  /**
   * Checks if a String is empty ("") or null.
   *
   * <pre>
   * StringUtils.isEmpty(null)      = true
   * StringUtils.isEmpty("")        = true
   * StringUtils.isEmpty(" ")       = false
   * StringUtils.isEmpty("bob")     = false
   * StringUtils.isEmpty("  bob  ") = false
   * </pre>
   *
   * NOTE: This method changed in Lang version 2.0.
   * It no longer trims the String.
   * That functionality is available in isBlank().
   *
   * @param str  the String to check, may be null
   * @return <code>true</code> if the String is empty or null
   */
  public static boolean isEmpty(String str) {
      return str == null || str.length() == 0;
  }
  
  
public static void main(String args[]){

System.out.println(StringUtils.substringBetween("<% cmd1.cmdRet==0; %>  sh exe.sh file='<% print(Date());%>.txt'", "<%", "%>"));
}

}
