 package org.tcoe.tutil;
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Chirgal Hansdah <chirgal@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


/**
 * Class that represents a command to be executed.
 *
 */
public class Cmd implements java.io.Serializable {

    private final String cmdAlias;
    private String cmdCndtn;
    private final String cmdStr;
    private String cmdOp;
    private int cmdRet;

    public Cmd(String cmdAlias, String cmdStr) {
        this.cmdAlias = cmdAlias;
        this.cmdCndtn = null;
        this.cmdStr = cmdStr;
        this.cmdOp = "";
        this.cmdRet = 0xffffd8f1;
    }

    public Cmd(String cmdAlias, String cmdCndtn, String cmdStr) {
        this.cmdAlias = cmdAlias;
        this.cmdCndtn = cmdCndtn;
        this.cmdStr = cmdStr;
        this.cmdOp = "";
        this.cmdRet = 0xffffd8f1;
    }

    public Cmd(String cmdAlias, String cmdStr, int cmdRet) {
        this.cmdAlias = cmdAlias;
        this.cmdStr = cmdStr;
        this.cmdOp = "";
        this.cmdRet = cmdRet;

    }

    public void setCmdRet(int cmdRet) {
        this.cmdRet = cmdRet;
    }

    public String getCmdAlias() {
        return cmdAlias;
    }

    public String getCmdStr() {
        return cmdStr;
    }

    public int getCmdRet() {
        return cmdRet;
    }

    public String getCmdCndtn() {
        return cmdCndtn;
    }

    public void setCmdOp(String cmdOp) {
        this.cmdOp = cmdOp;
    }

    public String getCmdOp() {
        return cmdOp;
    }

}
